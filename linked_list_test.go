package linked_list

import (
	"testing"
)

func TestList_PushFrontFirst(t *testing.T) {
	list := List{}
	list.PushFront(1)
	if list.First().Value() != 1 || list.Last().Value() != 1 {
		t.Errorf("Error, wrong value")

	}
	if list.First().Next() != nil || list.Last().Next() != nil {
		t.Errorf("Error, wrong Next link")
	}
	if list.First().Prev() != nil || list.Last().Prev() != nil {
		t.Errorf("Error, worng Prev link")
	}
}

func TestList_PushBackFirst(t *testing.T) {
	list := List{}
	list.PushBack(1)
	if list.First().Value() != 1 || list.Last().Value() != 1 {
		t.Errorf("Error, wrong value")
	}
	if list.First().Next() != nil || list.Last().Next() != nil {
		t.Errorf("Error, wrong Next link")
	}
	if list.First().Prev() != nil || list.Last().Prev() != nil {
		t.Errorf("Error, worng Prev link")
	}
}

func TestList_PushFront(t *testing.T) {
	list := List{}
	list.PushFront(1)
	list.PushFront(2)
	if list.First().Value() != 2 || list.Last().Value() != 1 {
		t.Errorf("Error, wrong value")
	}
	if list.First().Next() == nil || list.Last().Next() != nil {
		t.Errorf("Error, wrong Next link")
	}
	if list.First().Prev() != nil || list.Last().Prev() == nil {
		t.Errorf("Error, worng Prev link")
	}
}

func TestList_PushBack(t *testing.T) {
	list := List{}
	list.PushBack(1)
	list.PushBack(2)
	if list.First().Value() != 1 || list.Last().Value() != 2 {
		t.Errorf("Error, wrong value")
	}
	if list.First().Next() == nil || list.Last().Next() != nil {
		t.Errorf("Error, wrong Next link")
	}
	if list.First().Prev() != nil || list.Last().Prev() == nil {
		t.Errorf("Error, worng Prev link")
	}
}

func TestList_EmptyLen(t *testing.T) {
	list := List{}
	if list.Len() != 0 {
		t.Error("Wrong length for empty list")
	}
}

func TestList_Len(t *testing.T) {
	list := List{}
	list.PushFront(1)
	if list.Len() != 1 {
		t.Error("Wrong length for 1 element")
	}
	list.PushFront(2)
	if list.Len() != 2 {
		t.Error("Wrong length for 2 elements list")
	}
}

func TestList_RemoveElement(t *testing.T) {
	list := List{}
	list.PushBack(1)
	list.PushBack(2)
	list.PushBack(3)
	second := list.First().Next()
	list.Remove(second)
	if list.Len() != 2 {
		t.Error("Wrong length after element was removed")
	}
	if list.First().Next().Value() != 3 {
		t.Error("Wrong next value after element was removed")
	}
	if list.Last().Prev().Value() != 1 {
		t.Error("Wrong prev value after element was removed")
	}
}

func TestList_RemoveFirstElement(t *testing.T) {
	list := List{}
	list.PushBack(1)
	list.PushBack(2)
	list.PushBack(3)
	first := list.First()
	list.Remove(first)
	if list.Len() != 2 {
		t.Error("Wrong length after element was removed")
	}
	if list.First().Value() != 2 {
		t.Error("Wrong first value after element was removed")
	}
	if list.First().Next().Value() != 3 {
		t.Error("Wrong next value after element was removed")
	}
}

func TestList_RemoveLastElement(t *testing.T) {
	list := List{}
	list.PushBack(1)
	list.PushBack(2)
	list.PushBack(3)
	last := list.Last()
	list.Remove(last)
	if list.Len() != 2 {
		t.Error("Wrong length after element was removed")
	}
	if list.Last().Value() != 2 {
		t.Error("Wrong last value after element was removed")
	}
	if list.Last().Prev().Value() != 1 {
		t.Error("Wrong prev value after element was removed")
	}
}

func TestList_RemoveOnlyElement(t *testing.T) {
	list := List{}
	list.PushBack(1)
	last := list.Last()
	list.Remove(last)
	if list.Len() != 0 {
		t.Error("Wrong length after element was removed")
	}
	if list.Last() != nil {
		t.Error("Wrong last value after only element was removed")
	}
	if list.First() != nil {
		t.Error("Wrong first value after only element was removed")
	}
}
