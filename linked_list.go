package linked_list

// Item - double linked list element.
type Item struct {
	value interface{}
	next  *Item
	prev  *Item
}

// Value return value of the item
func (r *Item) Value() interface{} {
	return r.value
}

// Next return "right sibling" of the element
func (r *Item) Next() *Item {
	return r.next
}

// Next return "left sibling" of the element
func (r *Item) Prev() *Item {
	return r.prev
}

// List - double linked list, structure contains pointers to first and last elements
type List struct {
	first *Item
	last  *Item
}

// First - return pointer to the first element of the list
func (r *List) First() *Item {
	return r.first
}

// First - return pointer to the last element of the list
func (r *List) Last() *Item {
	return r.last
}

// PushBack add new element at the beginning of the list
func (r *List) PushFront(v interface{}) {
	newItem := &Item{value: v}
	if r.first == nil {
		r.first = newItem
		r.last = newItem
		return
	}
	newItem.next = r.first
	r.first.prev = newItem
	r.first = newItem
}

// PushBack add new element at the end of the list
func (r *List) PushBack(v interface{}) {
	newItem := &Item{value: v}
	if r.last == nil {
		r.first = newItem
		r.last = newItem
		return
	}
	newItem.prev = r.last
	r.last.next = newItem
	r.last = newItem
}

// Len return length of the list
// Complexity is O(n)
func (r *List) Len() int {
	if r.first == nil {
		return 0
	}

	length := 0
	for item := r.first; item != nil; item = item.next {
		length++
	}
	return length
}

// Remove - remove item from the list
func (r *List) Remove(i *Item) {
	if i.prev != nil {
		i.prev.next = i.next
	} else {
		r.first = i.next
	}

	if i.next != nil {
		i.next.prev = i.prev
	} else {
		r.last = i.prev
	}

	i.next = nil
	i.prev = nil
	i = nil
}
